from collections import deque
import numpy as np
from math import inf

import joblib
from binance.enums import *
from binance.exceptions import BinanceAPIException
from tensorflow.keras.models import load_model
from sqlalchemy import and_
from sqlalchemy.orm.exc import NoResultFound

from broker import client
from db import Session, Market, Investment

THRESHOLDS = [0.25, 0.4, 0.5, 1, 2, 9]
INVEST_TIMES = [2, 14, 120, 840, 2880, 20160]
AVG_COUNT = [1, 3, 10, 30, 60, 360]
COOLDOWN = [2, 5, 14, 120, 240, 1440]

def dummy(y_true, y_pred):
    return y_true - y_pred
custom_objects = {
    'max_ae': dummy,
    'mean_2m': dummy,
    'max_2m': dummy,
    'mean_14d': dummy,
    'max_14d': dummy,
    'sig_error_2m': dummy,
    'sig_error_14d': dummy,
    'mistake_2m': dummy,
    'mistake_14d': dummy,
    'mistake': dummy,
}

class Trader(object):
    def __init__(self, symbol, budget=0):
        self.symbol = symbol
        self.availableBudget = budget
        self.investments = []
        self.meta = {
            'dust': 0,
            'investAfter': [0, 0, 0, 0, 0, 0],
        }
        self.predictions = [deque(), deque(), deque(), deque(), deque(), deque()]
        self.scaler = joblib.load('models/%s-scaler.gz' % self.symbol)
        self.model = load_model('models/%s.hdf5' % self.symbol, custom_objects=custom_objects)
        self.__fetchInitialState()

    def __fetchInitialState(self):
        session = Session()
        try:
            market = session.query(Market).filter(Market.market == self.symbol).one()
            self.availableBudget = market.available_budget
            if market.meta:
                for field in market.meta:
                    if field in self.meta:
                        self.meta[field] = market.meta[field]
        except NoResultFound:
            market = Market(market=self.symbol, available_budget=self.availableBudget, meta=self.meta)
            session.add(market)
        session.commit()

    def __feed(self, inputs):
        inputs = self.scaler.transform(inputs)
        preds = self.model.predict(inputs) * 100
        for pred in preds:
            for i in range(6):
                self.predictions[i].append(pred[i])
                while len(self.predictions[i]) > AVG_COUNT[i]:
                    self.predictions[i].popleft()

    def initialize(self, bigState):
        self.__feed(np.array(bigState))

    def trade(self, time, price, marketState):
        self.__feed(np.array([marketState]))
        session = Session()
        self.investments = session.query(Investment).filter(Investment.market == self.symbol).all()
        buyQuote, sellQty, toAdd, toDelete, extensions = self.__makeDecision(time, price)
        if buyQuote or sellQty:
            sellQty += self.meta['dust']
            self.meta['dust'] = 0
            if sellQty >= buyQuote / price:
                sellQty -= buyQuote / price
                sellResult = self.__sell(sellQty)
                if sellResult is None:
                    print('[Trader-%s] Dust: %.5f' % (self.symbol, sellQty))
                    self.meta['dust'] = self.__round(sellQty, 5)
                else:
                    print('[Trader-%s] Sold %.5f and got $%.1f' % (self.symbol, sellQty, sellResult))
                    self.availableBudget += sellResult
            else:
                selfBuy = sellQty * price
                shouldBuy = buyQuote - selfBuy
                buyResult = self.__buy(shouldBuy) or 0
                fulfilment = (buyResult + sellQty) / (buyQuote / price)
                print('[Trader-%s] Bought %.5f from self and bought %.5f with $%.1f (%.2f%% fulfilled)' % (self.symbol, sellQty, buyResult, shouldBuy, fulfilment * 100))
                for investment in toAdd:
                    investment.qty = investment.qty * fulfilment
                if buyResult:
                    self.availableBudget -= shouldBuy
        session.add_all(toAdd + extensions)
        session.query(Investment).filter(and_(Investment.market == self.symbol, Investment.id.in_(list(toDelete)))).delete(synchronize_session=False)
        session.query(Market).filter(Market.market == self.symbol).update({'available_budget': self.availableBudget, 'meta': self.meta})
        session.commit()

    def __round(self, number, points):
        number *= 10 ** points
        number = int(number)
        return number / (10 ** points)

    def __buy(self, quote):
        try:
            quote = self.__round(quote, 2)
            order = client.create_order(symbol=self.symbol, side=SIDE_BUY, type=ORDER_TYPE_MARKET, quoteOrderQty=quote, recvWindow=60000)
            return float(order['executedQty'])
        except BinanceAPIException as e:
            print('[Trader-%s] Buy Exception: %s' % (self.symbol, str(e)))

    def __sell(self, qty):
        try:
            qty = self.__round(qty, 5)
            order = client.create_order(symbol=self.symbol, side=SIDE_SELL, type=ORDER_TYPE_MARKET, quantity=qty, recvWindow=60000)
            return float(order['cummulativeQuoteQty'])
        except BinanceAPIException as e:
            print('[Trader-%s] Sell Exception: %s' % (self.symbol, str(e)))

    def __makeDecision(self, time, price):
        preds = [0, 0, 0, 0, 0, 0]
        for i in range(6):
            if len(self.predictions[i]) == AVG_COUNT[i]:
                preds[i] = np.mean(self.predictions[i])
        print('[Trader-%s] Predictions: %s' % (self.symbol, list(map(lambda x: '%.2f%%' % x, preds))))

        shouldInvest = [p > t for (p, t) in zip(preds, THRESHOLDS)]
        shouldSell = [p < -t for (p, t) in zip(preds, THRESHOLDS)]
        sellQty, buyQuote = 0, 0
        toDelete = set()
        toAdd = []
        extensions = []

        sellQty += self.__sellIfLoss(time, price, preds, shouldInvest, shouldSell, toDelete)
        sellQty += self.__sellOrExtendExpired(time, price, preds, shouldInvest, toDelete, extensions)
        buyQuote += self.__invest(time, price, preds, shouldInvest, toAdd)

        return buyQuote, sellQty, toAdd, toDelete, extensions

    def __sellIfLoss(self, time, price, preds, shouldInvest, shouldSell, toDelete):
        sellQty = 0
        for i in range(6):
            if shouldSell[i]:
                ignore = False
                for j in range(i):
                    if shouldInvest[j]:
                        ignore = True
                if ignore:
                    continue
                for j in range(i, 6):
                    shouldInvest[j] = False
                due = time + INVEST_TIMES[i] * 60
                for investment in self.investments:
                    if investment.id not in toDelete and due <= investment.due and i <= investment.type:
                        self.meta['investAfter'][investment.type] = 0
                        profit = price * 100 / investment.buy_price - 100
                        sellQty += investment.qty
                        toDelete.add(investment.id)
                        if profit < 0.2:
                            print('[Trader-%s] WARNING: LOSS ON INVESTMENT!!!' % self.symbol)
                        print('[Trader-%s] Short-Sell (qty: %.5f) for $%.1f (%.2f%% Profit) because %.2f%% predicted in %d minutes.' % (self.symbol, investment.qty, investment.qty * price, profit, preds[i], INVEST_TIMES[i]))
        return sellQty

    def __sellOrExtendExpired(self, time, price, preds, shouldInvest, toDelete, toAdd):
        sellQty = 0
        for investment in self.investments:
            if investment.due <= time and investment.id not in toDelete:
                self.meta['investAfter'][investment.type] = 0
                realProfit = price * 100 / investment.buy_price - 100
                print('[Trader-%s] %d minute investment Due. Expected Profit: %.2f%%, Real Profit: %.2f%%' % (self.symbol, INVEST_TIMES[investment.type], investment.expected_profit, realProfit))
                extension = None
                profit = 0
                for i in range(6):
                    if shouldInvest[i] and preds[i] > profit:
                        extension = i
                        profit = preds[i]
                toDelete.add(investment.id)
                if extension is None:
                    sellQty += investment.qty
                    if realProfit < 0.2:
                        print('[Trader-%s] WARNING: LOSS ON INVESTMENT!!!' % self.symbol)
                    print('[Trader-%s] Sell (qty: %.5f) for $%.1f (%.2f%% Profit)' % (self.symbol, investment.qty, investment.qty * price, realProfit))
                else:
                    extensionDue = time + INVEST_TIMES[extension] * 60
                    expectedProfit = price * (1 + profit / 100) * 100 / investment.buy_price - 100
                    newInvestment = Investment(market=self.symbol, qty=investment.qty, buy_price=investment.buy_price, expected_profit=expectedProfit, due=extensionDue, type=extension)
                    toAdd.append(newInvestment)
                    print('[Trader-%s] Extend (qty: %.5f) for %d minutes (Was Expected: %.2f%%, Now: %.2f%%, Is Expected: %.2f%%)' % (self.symbol, investment.qty, INVEST_TIMES[extension], investment.expected_profit, realProfit, expectedProfit))
        return sellQty

    def __invest(self, time, price, preds, shouldInvest, toAdd):
        buyQuote = 0
        eachShare = max(10, 0.05 * self.availableBudget)
        for i in range(1, 6):
            if self.meta['investAfter'][i] <= time and shouldInvest[i] and self.availableBudget - buyQuote >= 10:
                if self.availableBudget - buyQuote < eachShare:
                    eachShare = max(10, 0.05 * (self.availableBudget - buyQuote))
                numShares = min(preds[i] // THRESHOLDS[i], (self.availableBudget - buyQuote) / eachShare)
                totalBuy = numShares * eachShare
                if self.availableBudget - buyQuote - totalBuy < 10:
                    totalBuy = self.availableBudget - buyQuote
                buyQuote += totalBuy
                due = time + INVEST_TIMES[i] * 60
                self.meta['investAfter'][i] = time + COOLDOWN[i] * 60
                newInvestment = Investment(market=self.symbol, qty=totalBuy / price, buy_price=price, expected_profit=preds[i], due=due, type=i)
                toAdd.append(newInvestment)
                print('[Trader-%s] Buy (qty: %.5f) with $%.1f for %d minutes (Expected Profit: %.2f%%)' % (self.symbol, totalBuy / price, totalBuy, INVEST_TIMES[i], preds[i]))
        return buyQuote
