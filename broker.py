from binance.client import Client
import json

secret = json.loads(open('secret.json').read())
client = Client(secret['binance_api_key'], secret['binance_api_secret'])
