import time
from datetime import datetime

from sqlalchemy.sql import func

from collector import Collector
from trader import Trader
from db import Session, MoneyHistory, Market, Investment

def run():
    markets = ['ETHUSDT', 'BTCUSDT']
    collectors = {}
    traders = {}
    for market in markets:
        collectors[market] = Collector(market)
        traders[market] = Trader(market)

    # Initialize
    for market in markets:
        collectors[market].update()
        traders[market].initialize(collectors[market].marketState)

    # Loop
    lastUpdate = 0
    while True:
        lastUpdate = time.time()

        hasUpdate = []
        for market in markets:
            hasUpdate.append(collectors[market].update())

        for market, fresh in zip(markets, hasUpdate):
            if fresh:
                traders[market].trade(collectors[market].lastTime, collectors[market].lastPrice, collectors[market].marketState[-1])

        curTime = datetime.utcnow()
        if curTime.hour == 0 and curTime.minute == 0:
            curTime = curTime.replace(second=0, microsecond=0)
            print('[Main] Creating money history')
            session = Session()
            for market in markets:
                availableQuote = session.query(Market.available_budget).filter(Market.market == market).scalar()
                qty = session.query(func.sum(Investment.qty)).filter(Investment.market == market).scalar() or 0
                qty += traders[market].meta['dust']
                price = collectors[market].lastPrice
                if price is None:
                    continue
                newStat = MoneyHistory(market=market, timestamp=int(curTime.timestamp()), qty=qty, quote=availableQuote, total_quote=availableQuote + qty * price)
                session.add(newStat)
            session.commit()

        remainingTime = (lastUpdate + 60) - time.time() - 0.5
        if remainingTime:
            print('[Main] Sleeping %d seconds @ time %d' % (int(remainingTime), int(time.time())))
            time.sleep(remainingTime)

if __name__ == '__main__':
    run()
