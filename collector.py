from datetime import datetime, timedelta
from collections import deque
import time

from binance.exceptions import BinanceAPIException
from binance.enums import *
from sqlalchemy import desc, asc, and_
from sqlalchemy.sql import func
from sqlalchemy.orm.exc import NoResultFound
from requests.exceptions import ConnectionError

from broker import client
from db import Session, KLine

class Collector(object):
    def __init__(self, symbol, startTime=None):
        self.symbol = symbol
        self.selectedStartTime = startTime
        self.marketState = deque()
        self.lastPrice = None
        self.__getLastTime()

    def __getLastTime(self):
        session = Session()
        self.firstTime = session.query(KLine.timestamp).filter(KLine.market == self.symbol).order_by(asc(KLine.timestamp)).first()
        self.lastTime = session.query(KLine.timestamp).filter(KLine.market == self.symbol).order_by(desc(KLine.timestamp)).first()
        if self.lastTime is None:
            self.lastTime = int(((datetime.utcnow() - timedelta(days=14, hours=6)).replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(minutes=1)).timestamp())
            self.firstTime = self.lastTime + 60
            if self.selectedStartTime:
                self.lastTime = self.selectedStartTime - 60
        else:
            self.lastTime = self.lastTime[0]
            self.firstTime = self.firstTime[0]
        self.processFrom = self.firstTime + 14 * 24 * 3600
        session.close()

    def __addOne(self, session, kline):
        newKLine = KLine(market=self.symbol, timestamp=kline[0] // 1000, open=float(kline[1]), high=float(kline[2]), low=float(kline[3]), close=float(kline[4]), volume=float(kline[5]), buy_volume=float(kline[9]), avg_price=float(kline[7]) / float(kline[5]) if float(kline[5]) != 0 else None)
        self.lastTime = newKLine.timestamp
        self.lastPrice = newKLine.close
        session.add(newKLine)
        if newKLine.timestamp >= self.processFrom:
            self.__processOne(session, newKLine)

    def __processOne(self, session, kline):
        if len(self.marketState) == 0:
            self.__processOneFromScratch(session, kline)
            return

        newState = [0] * 84
        idx = 0
        for period, period_length in [('m', 60), ('h', 3600), ('d', 86400)]:
            for duration in range(1, 15):
                try:
                    matchingKLine = session.query(KLine).filter(and_(KLine.market == self.symbol, KLine.timestamp == kline.timestamp - (duration * period_length))).one()

                    # Update Diff
                    newState[idx] = (matchingKLine.close / kline.close) - 1

                    # Update Vol
                    newState[idx + 42] = self.marketState[-1][idx + 42] - matchingKLine.volume + kline.volume
                except NoResultFound:
                    pastPrice, volume = self.__getFromScratchDiffVol(session, kline.timestamp - duration * period_length, kline.timestamp)
                    newState[idx] = (pastPrice / kline.close) - 1
                    newState[idx + 42] = volume
                idx += 1

        self.marketState.append(newState)
        while len(self.marketState) > 360:
            self.marketState.popleft()

    def __processOneFromScratch(self, session, kline):
        print('[Collector-%s] Performing from scratch state calculation for time %d' % (self.symbol, kline.timestamp))
        newState = [0] * 84
        idx = 0
        for period, period_length in [('m', 60), ('h', 3600), ('d', 86400)]:
            for duration in range(1, 15):
                pastPrice, volume = self.__getFromScratchDiffVol(session, kline.timestamp - duration * period_length, kline.timestamp)

                # Update Diff
                newState[idx] = (pastPrice / kline.close) - 1

                # Update Vol
                newState[idx + 42] = volume + kline.volume

                idx += 1
        self.marketState.append(newState)

    def __getFromScratchDiffVol(self, session, timestamp, now):
        while True:
            pastPrice = session.query(KLine.close).filter(and_(KLine.market == self.symbol, KLine.timestamp == timestamp)).scalar()
            volume = session.query(func.sum(KLine.volume)).filter(and_(KLine.market == self.symbol, KLine.timestamp > timestamp, KLine.timestamp < now)).scalar()
            volume = 0 if volume is None else int(volume)
            if pastPrice is None:
                oldTime = timestamp
                timestamp = self.__getClosestTime(session, timestamp)
                print('[Collector-%s] Inaccuracy: %d minutes for a %d-minute period' % (self.symbol, (timestamp - oldTime) // 60, (now - oldTime) // 60))
            else:
                return pastPrice, volume

    def __getClosestTime(self, session, timestamp):
        futureTime = session.query(KLine.timestamp).filter(and_(KLine.market == self.symbol, KLine.timestamp > timestamp)).order_by(asc(KLine.timestamp)).limit(1).scalar()
        pastTime = session.query(KLine.timestamp).filter(and_(KLine.market == self.symbol, KLine.timestamp < timestamp)).order_by(desc(KLine.timestamp)).limit(1).scalar()
        return futureTime if abs(futureTime - timestamp) <= abs(pastTime - timestamp) else pastTime

    def __fillMarketState(self):
        numNeeded = 360 - len(self.marketState)
        if not numNeeded: return
        curFirst = self.lastTime - 60 * (len(self.marketState) - 1)
        needFirst = curFirst - numNeeded * 60
        oldState = self.marketState
        self.marketState = deque()
        session = Session()
        result = session.query(KLine).filter(and_(KLine.market == self.symbol, KLine.timestamp >= needFirst, KLine.timestamp < curFirst)).all()
        for kline in result:
            self.__processOne(session, kline)
        session.close()
        self.marketState.extend(oldState)

    def update(self):
        oldLast = self.lastTime
        while True:
            session = Session()
            while True:
                try:
                    result = client.get_klines(symbol=self.symbol, startTime=(self.lastTime + 60) * 1000, interval=KLINE_INTERVAL_1MINUTE, limit=1000)
                    break
                except Exception:
                    print('[Collector-%s] Retrying on error' % self.symbol)
                    time.sleep(1)
                    continue
            print('[Collector-%s] Collecting %d klines starting from %d from %s market' % (self.symbol, len(result), (self.lastTime + 60), self.symbol))
            for kline in result:
                self.__addOne(session, kline)
            session.commit()
            if len(result) < 1000:
                break
        self.__fillMarketState()
        return self.lastTime != oldLast
