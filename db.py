from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String, REAL, JSON
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

#engine = create_engine("mysql://root:root@localhost", echo=True)
engine = create_engine("mysql://root:root@localhost")
engine.execute("SET GLOBAL innodb_file_per_table=ON")
engine.execute("SET GLOBAL innodb_default_row_format='dynamic'")
engine.execute("SET GLOBAL innodb_compression_algorithm='lzma'")
engine.execute("SET GLOBAL innodb_compression_default=ON")
engine.execute("SET GLOBAL innodb_compression_level=9")
engine.execute("CREATE DATABASE IF NOT EXISTS cryptotrade")
engine.execute("USE cryptotrade")

metadata = MetaData(bind=engine)
Base = declarative_base(metadata=metadata)
class KLine(Base):
    __tablename__ = 'klines'

    market = Column(String(16), primary_key=True)
    timestamp = Column(Integer, primary_key=True)
    open = Column(REAL)
    close = Column(REAL)
    low = Column(REAL)
    high = Column(REAL)
    volume = Column(REAL)
    buy_volume = Column(REAL)
    avg_price = Column(REAL)

class Market(Base):
    __tablename__ = 'markets'

    market = Column(String(16), primary_key=True)
    available_budget = Column(REAL)
    meta = Column(JSON)

class Investment(Base):
    __tablename__ = 'investments'

    market = Column(String(16), primary_key=True)
    id = Column(Integer, primary_key=True, autoincrement=True)
    qty = Column(REAL)
    buy_price = Column(REAL)
    expected_profit = Column(REAL)
    due = Column(Integer)
    type = Column(Integer)

class MoneyHistory(Base):
    __tablename__ = 'money_history'

    market = Column(String(16), primary_key=True)
    timestamp = Column(Integer, primary_key=True)
    qty = Column(REAL)
    quote = Column(REAL)
    total_quote = Column(REAL)

Base.metadata.create_all(engine)

Session = sessionmaker(bind=engine)
